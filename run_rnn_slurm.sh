#!/bin/bash

#SBATCH -q gpu
#SBATCH -g 1
#SBATCH -t 30:00

module load cuda
module load gcc/4.5.1

THEANO_FLAGS="floatX=float32,device=gpu0,cxx=/gpfs/runtime/opt/gcc/4.5.1/bin/g++" python main.py
